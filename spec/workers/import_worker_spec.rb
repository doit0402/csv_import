require 'spec_helper'

describe ImportWorker, type: :worker do
  subject{ ImportWorker.perform_async('Supplier', import.id) }

  let(:import) {Import.create(file: file)}
  let(:file) { fixture_file_upload('suppliers.csv') }

  it { expect{ subject }.to change(ImportWorker.jobs, :size).by(1) }
end
