# frozen_string_literal: true

require 'rails_helper'

describe Sku, type: :model do
  describe 'relations' do
    it { is_expected.to belong_to(:supplier).with_foreign_key(:supplier_code).with_primary_key(:supplier_code) }
  end

  describe 'validations' do
    it {is_expected.to validate_presence_of(:sku)}
    it {is_expected.to validate_presence_of(:supplier_code)}
    it {is_expected.to validate_presence_of(:name)}
    it {is_expected.to validate_presence_of(:price)}
  end

  describe '#import' do

    context 'with correct file' do
      subject{ Sku.import(file) }

      let(:file) { fixture_file_upload('sku.csv') }

      before { Supplier.create(supplier_code: 2437, name: 'name') }

      it { expect{ subject }.to change{ Sku.count }.by(4) }
    end

    context 'without related suppliers' do
      subject{ Sku.import(file) }

      let(:file) { fixture_file_upload('sku.csv') }

      it { expect{ subject }.to raise_error(ActiveRecord::RecordInvalid) }
      it { expect{ subject rescue nil }.not_to change{ Supplier.count } }
    end

    context 'with incorrect file' do
      subject{ Sku.import(file) }

      let(:file) { fixture_file_upload('wrong_sku.csv') }

      before { Supplier.create(supplier_code: 2437, name: 'name') }

      it { expect{ subject }.to raise_error(ActiveRecord::RecordInvalid) }
      it { expect{ subject rescue nil }.not_to change{ Supplier.count } }
    end

  end
end
