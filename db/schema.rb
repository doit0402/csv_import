# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_06_06_192105) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "imports", force: :cascade do |t|
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "skus", force: :cascade do |t|
    t.string "sku", null: false
    t.string "supplier_code", null: false
    t.string "name", null: false
    t.string "additional_field_1"
    t.string "additional_field_2"
    t.string "additional_field_3"
    t.string "additional_field_4"
    t.string "additional_field_5"
    t.decimal "price", null: false
    t.index ["sku"], name: "index_skus_on_sku", unique: true
  end

  create_table "suppliers", force: :cascade do |t|
    t.string "supplier_code", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["supplier_code"], name: "index_suppliers_on_supplier_code", unique: true
  end

end
