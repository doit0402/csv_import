class CreateSkus < ActiveRecord::Migration[5.2]
  def change
    create_table :skus do |t|
      t.string :sku, null: false
      t.string :supplier_code, null: false
      t.string :name, null: false
      t.string :additional_field_1
      t.string :additional_field_2
      t.string :additional_field_3
      t.string :additional_field_4
      t.string :additional_field_5
      t.decimal :price, null: false
    end

    add_index :skus, :sku, unique: true
  end
end
