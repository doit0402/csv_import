class ImportWorker
  include Sidekiq::Worker
  def perform(model, import_id)
    import = Import.find(import_id)
    model.constantize.import(import.file)
  end
end
