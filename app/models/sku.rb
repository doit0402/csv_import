# frozen_string_literal: true

class Sku < ApplicationRecord
  belongs_to :supplier, foreign_key: :supplier_code, primary_key: :supplier_code
  validates :sku, uniqueness: true
  validates :sku, :supplier_code, :name, :price, presence: true

  def self.import(file)
    ActiveRecord::Base.transaction do
      CSV.foreach(file.path, col_sep: '¦') do |row|
        Sku.create!(
          sku: row[0],
          supplier_code: row[1],
          name: row[2],
          additional_field_1: row[3],
          additional_field_2: row[4],
          additional_field_3: row[5],
          additional_field_4: row[6],
          additional_field_5: row[7],
          price: row[8]
        )
      end
    end
  end
end
