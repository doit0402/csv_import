# frozen_string_literal: true

class Supplier < ApplicationRecord
  has_many :skus, foreign_key: :supplier_code, primary_key: :supplier_code

  validates :supplier_code, uniqueness: true
  validates :supplier_code, :name, presence: true

  def self.import(file)
    ActiveRecord::Base.transaction do
      CSV.foreach(file.path, col_sep: '¦') do |row|
        Supplier.create!(
          supplier_code: row[0],
          name: row[1]
        )
      end
    end
  end
end
