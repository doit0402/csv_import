Rails.application.routes.draw do
  root to: 'main#index'
  resources :suppliers, only: %i[index create]
  resources :skus, only: %i[index create]
end
